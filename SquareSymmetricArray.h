#ifndef SQUARESYMMETRICARRAY_H
#define SQUARESYMMETRICARRAY_H

// Saves half memory compared to simple N*N symmetric array

#include <vector>
#include <algorithm>
#include <cstdlib>

template<class T>
class SquareSymmetricArray
{
public:
	SquareSymmetricArray(int n)
	{
		arr.resize(n);
		for (int i = 0; i < n; i++)
			arr[i].resize(n - i);
	}
	
public:
	T Get(int i, int j) const
	{
		return arr[std::min(i, j)][abs(j - i)];
	}

	void Put(int i, int j, T val)
	{
		arr[std::min(i, j)][abs(j - i)] = val;
	}

private:
	std::vector<std::vector<T>> arr;
};

#endif /* SQUARESYMMETRICARRAY_H */