#include "NetworkProcessor.h"

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		std::cout << "Usage:" << std::endl;
		std::cout << argv[0] << " <input_file.corr> <threshold> [-q]" << std::endl;
		return 1;
	}

	std::string corrfile = argv[1];
	double threshold = atof(argv[2]);
	bool queryOnly = (argc > 3 && std::string(argv[3]) == "-q");

	NetworkProcessor proc(corrfile, threshold, queryOnly);

	return 0;
}
