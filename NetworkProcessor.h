#ifndef NETWORKPROCESSOR_H
#define NETWORKPROCESSOR_H

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <queue>
#include <unordered_set>
#include <memory>
#include <thread>
#include <mutex>
#include <locale>
#include "Model.h"
#include "SquareSymmetricArray.h"

class NetworkProcessor
{
public:
	NetworkProcessor(const std::string &corrfile, double threshold, bool queryOnly);

	void ReadCorrFile();
	bool CheckQueryOnly();

	void MakeAdjacencyMatrix();
	void MakeAdjacencyMatrixParallel(int id);

	void CalculateDegreeDistribution();

	void CalculateClusteringCoefficient();
	void CalculateClusteringCoefficientParallel(int id);

	void CalculateAverageShortestPathLength();
	void CalculateAverageShortestPathLengthParallel(int id);

	void Export();
	void ExportAverageShortestPaths(std::ofstream &out);
	void ExportDegreeDistribution(std::ofstream &out);
	void ExportClusteringAgainstDegreeByCount(std::ofstream &out);
	void ExportClusteringAgainstDegreeByTotal(std::ofstream &out);

private:
	std::string corrfile;
	double threshold;
	bool queryOnly;

	Model model;
	int nodes, nodesNonTrivial;
	int edgesCount;
	int diameter;
	int componentsCount, nonTrivialComponentsCount;
	double density;
	double averageDeg;
	double clusteringCoeff;
	double averageShortestPath;
	std::unique_ptr<SquareSymmetricArray<float>> corr;
	std::vector<std::unordered_set<int>> adjList;
	std::map<int, int> degreeDistribution;
	std::map<int, double> clusteringDistribution;
	std::vector<double> nodesClustering;
	std::vector<std::vector<int>> lengths;
	std::vector<std::pair<int, double>> averageShortestPathInNonTrivialComponents;

	std::vector<std::unique_ptr<std::mutex>> locks;

private:
	template<class T>
	static int sign(T x)
	{
		if (x < 0)
			return -1;
		else if (x > 0)
			return 1;
		return 0;
	}
};

#endif /* NETWORKPROCESSOR_H */