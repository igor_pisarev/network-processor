CXX=g++
LIBS = -pthread
CXXFLAGS=-g -std=c++14 -Wall -O2 -pedantic
LFLAGS=-g
BIN=process

SRC=$(wildcard *.cpp)
OBJ=$(SRC:%.cpp=%.o)

all: $(BIN)

$(BIN): $(OBJ)
	$(CXX) $(LFLAGS) -o $@ $^ $(LIBS)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $<

clean:
	rm -f *.o
	rm $(BIN)
