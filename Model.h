#ifndef MODEL_H
#define MODEL_H

#include <string>

enum class ModelType
{
	RegularLattice,
	SmallWorldRewiring,
	SmallWorldAddLinksRandomly,
	SmallWorldAddLinksPreferably,
};

struct Model
{
	ModelType modelType;
	int n;
	double temperature;
	int iterations;
	double probability;

	std::string GetType() const
	{
		switch (modelType)
		{
		case ModelType::RegularLattice:
			return "RegularLattice";
		case ModelType::SmallWorldRewiring:
			return "SmallWorldRewiring";
		case ModelType::SmallWorldAddLinksRandomly:
			return "SmallWorldAddLinksRandomly";
		case ModelType::SmallWorldAddLinksPreferably:
			return "SmallWorldAddLinksPreferably";
		default:
			return "";
		}
	}
};

#endif /* MODEL_H */