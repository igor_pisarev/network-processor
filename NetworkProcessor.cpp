#include "NetworkProcessor.h"

namespace
{
const int INF = 1 << 30;
const int NUM_THREADS = std::thread::hardware_concurrency();
}

NetworkProcessor::NetworkProcessor(const std::string &corrfile, double threshold, bool queryOnly)
	: corrfile(corrfile),
	threshold(threshold),
	queryOnly(queryOnly)
{
	if (corrfile.empty())
		return;

	ReadCorrFile();
	if (CheckQueryOnly())
		return;

	locks.resize(nodes);
	for (int i = 0; i < nodes; i++)
		locks[i] = std::make_unique<std::mutex>();

	MakeAdjacencyMatrix();
	corr.reset(nullptr);

	nodesNonTrivial = 0;
	for (int i = 0; i < nodes; i++)
		if (adjList[i].size())
			nodesNonTrivial++;

	CalculateDegreeDistribution();
	CalculateClusteringCoefficient();
	CalculateAverageShortestPathLength();

	Export();
}

void NetworkProcessor::ReadCorrFile()
{
	std::ifstream inp(corrfile.c_str(), std::ios::in | std::ios::binary);

	// read model data
	int modelType;
	inp.read(reinterpret_cast<char*>(&modelType), sizeof(int));
	model.modelType = (ModelType)modelType;
	inp.read(reinterpret_cast<char*>(&model.n), sizeof(int));
	inp.read(reinterpret_cast<char*>(&model.temperature), sizeof(double));
	inp.read(reinterpret_cast<char*>(&model.iterations), sizeof(int));
	if (model.modelType != ModelType::RegularLattice)
		inp.read(reinterpret_cast<char*>(&model.probability), sizeof(double));
	inp.read(reinterpret_cast<char*>(&nodes), sizeof(int));
	if (queryOnly)
		return;

	// read correlation matrix
	corr = std::make_unique<SquareSymmetricArray<float>>(nodes);
	float c;
	for (int i = 0; i < nodes; i++)
	{
		for (int j = i + 1; j < nodes; j++)
		{
			inp.read(reinterpret_cast<char*>(&c), sizeof(float));
			corr->Put(i, j, c);
		}
	}
}

bool NetworkProcessor::CheckQueryOnly()
{
	if (!queryOnly)
		return false;
	
	std::cout << "-- query file only --" << std::endl;
	std::cout << "model type: " << model.GetType() << std::endl;
	std::cout << "n: " << model.n << std::endl;
	std::cout << "temperature: " << model.temperature << std::endl;
	std::cout << "iterations: " << model.iterations << std::endl;
	if (model.modelType != ModelType::RegularLattice)
		std::cout << "probability: " << model.probability << std::endl << std::endl;

	std::cout << "nodes: " << nodes << std::endl;
	return true;
}

void NetworkProcessor::MakeAdjacencyMatrix()
{
	std::cout << "Building network by thresholding correlation matrix..." << std::flush;
	adjList.resize(nodes);

	std::vector<std::thread> tasks;
	for (int i = 0; i < NUM_THREADS; i++)
		tasks.push_back(std::thread(&NetworkProcessor::MakeAdjacencyMatrixParallel, this, i));
	for (int i = 0; i < NUM_THREADS; i++)
		tasks[i].join();

	edgesCount = 0;
	for (int i = 0; i < nodes; i++)
		edgesCount += adjList[i].size();
	edgesCount /= 2;
	density = (double)edgesCount / ((nodes * (nodes - 1)) / 2);
}

void NetworkProcessor::MakeAdjacencyMatrixParallel(int id)
{
	for (int i = 0; i < nodes; i++)
	{
		for (int j = i + 1 + id; j < nodes; j += NUM_THREADS)
		{
			bool add = false;
			float x = corr->Get(i, j);
			if (threshold == 0)
				add = true; // make complete graph

			if (abs(x) >= abs(threshold) && sign(x) == sign(threshold))
				add = true;

			if (add)
			{
				{
					std::lock_guard<std::mutex> autolock(*locks[i]);
					adjList[i].insert(j);
				}
				{
					std::lock_guard<std::mutex> autolock(*locks[j]);
					adjList[j].insert(i);
				}
			}
		}

		// printing approximate progress
		if (id == 0 && (i % (nodes / 10) == 0 || i == nodes - 1))
			std::cout << '+' << std::flush;
	}
}


void NetworkProcessor::CalculateDegreeDistribution()
{
	int degSum = 0;
	for (int i = 0; i < nodes; i++)
	{
		int cnt = adjList[i].size();
		degreeDistribution[cnt]++;
		degSum += cnt;
	}
	averageDeg = (double)degSum / nodes;
}


void NetworkProcessor::CalculateClusteringCoefficient()
{
	std::cout << "Calculating clustering: " << std::flush;
	nodesClustering.resize(nodes);

	std::vector<std::thread> tasks;
	for (int i = 0; i < NUM_THREADS; i++)
		tasks.push_back(std::thread(&NetworkProcessor::CalculateClusteringCoefficientParallel, this, i));
	for (int i = 0; i < NUM_THREADS; i++)
		tasks[i].join();

	clusteringCoeff = 0;
	for (int i = 0; i < nodes; i++)
		clusteringCoeff += nodesClustering[i];
	clusteringCoeff /= nodes;

	for (int i = 0; i < nodes; i++)
		clusteringDistribution[adjList[i].size()] += nodesClustering[i];
}

void NetworkProcessor::CalculateClusteringCoefficientParallel(int id)
{
	for (int node = id; node < nodes; node += NUM_THREADS)
	{
		int neighbours = adjList[node].size();
		long linksTotal = (neighbours * (neighbours - 1));
		if (linksTotal == 0)
		{
			nodesClustering[node] = 0;
			continue;
		}

		long linksPresent = 0;
		for (const auto& ni : adjList[node])
			for (const auto& nj : adjList[node])
				if (adjList[ni].find(nj) != adjList[ni].cend())
					linksPresent++;

		nodesClustering[node] = (double)linksPresent / linksTotal;

		if (id == 0 && (node % (nodes / 10) == 0 || node == nodes - 1))
			std::cout << '+' << std::flush;
	}
}


void NetworkProcessor::CalculateAverageShortestPathLength()
{
	std::cout << "Calculating av.shortest paths: " << std::flush;
	lengths.resize(nodes);
	for (int i = 0; i < nodes; i++)
	{
		lengths[i].resize(nodes, INF);
		lengths[i][i] = 0;
	}

	std::vector<std::thread> tasks;
	for (int i = 0; i < NUM_THREADS; i++)
		tasks.push_back(std::thread(&NetworkProcessor::CalculateAverageShortestPathLengthParallel, this, i));
	for (int i = 0; i < NUM_THREADS; i++)
		tasks[i].join();

	// calculate average shortest path length in connected components independently
	// then take arithmetic mean of them
	std::vector<std::vector<int>> components;
	std::vector<bool> belongs(nodes, false);
	for (int i = 0; i < nodes; i++)
	{
		if (belongs[i])
			continue;

		belongs[i] = true;
		int componentInd = components.size();
		components.push_back(std::vector<int>());
		components[componentInd].push_back(i);

		for (int j = 0; j < nodes; j++)
		{
			if (i == j || lengths[i][j] == INF)
				continue;

			belongs[j] = true;
			components[componentInd].push_back(j);
		}
	}

	averageShortestPath = 0;
	componentsCount = components.size();
	nonTrivialComponentsCount = 0;
	if (componentsCount == 0)
		return;

	diameter = 0;
	for (auto&& comp : components)
	{
		if (comp.size() < 2)
			continue;

		nonTrivialComponentsCount++;
		long componentShortestPathSum = 0;
		for (size_t i = 0; i < comp.size(); i++)
		{
			for (size_t j = i + 1; j < comp.size(); j++)
			{
				int l = lengths[comp[i]][comp[j]];
				componentShortestPathSum += l;

				if (diameter < l)
					diameter = l;
			}
		}

		long pathsCount = (comp.size() * (comp.size() - 1)) / 2;
		double averageShortestPathInComp = (double)componentShortestPathSum / pathsCount;

		averageShortestPathInNonTrivialComponents.push_back(std::make_pair(comp.size(), averageShortestPathInComp));
		averageShortestPath += averageShortestPathInComp;
	}

	averageShortestPath /= nonTrivialComponentsCount;
}

void NetworkProcessor::CalculateAverageShortestPathLengthParallel(int id)
{
	for (int root = id; root < nodes; root += NUM_THREADS)
	{
		std::queue<int> q;
		q.push(root);
		while (q.size())
		{
			int curr = q.front();
			q.pop();
			for (int next : adjList[curr])
			{
				if (lengths[root][next] != INF)
					continue;

				lengths[root][next] = lengths[root][curr] + 1;
				q.push(next);
			}
		}

		if (id == 0 && (root % (nodes / 10) == 0 || root == nodes - 1))
			std::cout << '+' << std::flush;
	}
}

void NetworkProcessor::Export()
{
	std::string filename = corrfile.substr(0, corrfile.rfind('.')) + ".csv";
	std::ofstream out(filename.c_str());
	
	out.imbue(std::locale(""));

	out << "Input parameters:" << std::endl;
	out << "model type;" << model.GetType() << std::endl;
	out << "lattice;" << model.n << std::endl;
	out << "nodes;" << nodes << std::endl;
	out << "nodes not 0;" << nodesNonTrivial << std::endl;
	out << "iterations;" << model.iterations << std::endl;
	out << std::endl;
	out << "temperature;" << model.temperature << std::endl;
	if (model.modelType != ModelType::RegularLattice)
		out << "probability;" << model.probability << std::endl;
	out << std::endl;
	out << "threshold;" << threshold << std::endl;
	out << std::endl << std::endl;
	out << "Calculated properties:" << std::endl;
	out << "edges;" << edgesCount << std::endl;
	out << "components;" << componentsCount << std::endl;
	out << "non trivial;" << nonTrivialComponentsCount << std::endl;
	out << "density;" << density << std::endl;
	out << "average degree;" << averageDeg << std::endl;
	out << std::endl;
	out << "av.shortest path;" << averageShortestPath << std::endl;
	out << "diameter;" << diameter << std::endl;
	out << "clustering;" << clusteringCoeff << std::endl;
	out << std::endl;
	ExportAverageShortestPaths(out);
	out << std::endl;
	ExportDegreeDistribution(out);
	out << std::endl;
	out << std::endl;
	ExportClusteringAgainstDegreeByCount(out);
	out << std::endl;
	ExportClusteringAgainstDegreeByTotal(out);
}

void NetworkProcessor::ExportAverageShortestPaths(std::ofstream &out)
{
	out << "Comp.size;Av.sh.path" << std::endl;
	for (auto&& item : averageShortestPathInNonTrivialComponents)
		out << item.first << ";" << item.second << std::endl;
}

void NetworkProcessor::ExportDegreeDistribution(std::ofstream &out)
{
	out << "Degree distribution:" << std::endl;
	for (auto&& item : degreeDistribution)
		out << item.first << ";" << ((double)item.second / nodes) << std::endl;
}

void NetworkProcessor::ExportClusteringAgainstDegreeByCount(std::ofstream &out)
{
	out << "Degree;cluster.coeff;(normalized by count)" << std::endl;
	for (auto&& item : clusteringDistribution)
		out << item.first << ";" << (item.second / adjList[item.first].size()) << std::endl;
}

void NetworkProcessor::ExportClusteringAgainstDegreeByTotal(std::ofstream &out)
{
	out << "Degree;cluster.coeff;(normalized by total)" << std::endl;
	for (auto&& item : clusteringDistribution)
		out << item.first << ";" << (item.second / nodes) << std::endl;
}
